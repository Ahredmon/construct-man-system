﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CMS.DataAccess
{
    public class EFAccess
    {
         ConstructManDB db = new ConstructManDB();

        /// <summary>
        /// Accessors for all  - relevant- information in the database.
        /// </summary>
        public List<Task> GetEmployeeTasks()
        {
            return db.Tasks.ToList();
        }
        public List<Person> GetEmployees()
        {
            return db.People.ToList();
        }
        public List<Detail> GetEmployeeDetails()
        {
            return db.Details.ToList();
        }
        public List<Status> GetStatuses()
        {
            return db.Status.ToList();
        }
        public List<AcceptedProject> GetProjects()
        {
            return db.AcceptedProjects.ToList();
        }
        public List<WorkDay> GetWorkDays()
        {
            return db.WorkDays.ToList();
        }
        public List<Task1> GetProjectTasks()
        {
            return db.Task1.ToList();
        }

        public List<Site> GetProjectSites()
        {
            return db.Sites.ToList();
        }

        public List<Schedule> GetSchedules(int id)
        {
            return db.Schedules.Where(e=> e.EmployeeID == id).ToList();
        }

        public List<Role> GetRoles()
        {
            return db.Roles.ToList();
        }
        /// creation methods
        public bool CreateEmployee(Person p,Detail d)
        {
            try
            {
                db.People.Add(p).Detail = d;
                SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CreateTask(Task t)
        {
            try
            {
                t.Task1 = db.Task1.First(e => e.Id == t.ProjectId);
                t.DateModified = DateTime.Now;
                db.Tasks.Add(t);
                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool CreateDetail(Detail d)
        {
            try
            {
                db.Details.Add(d);
                SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CreateStatus(Status s)
        {
            try
            {
                db.Status.Add(s);
                SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool CreateWorkday(WorkDay w)
        {
            
            try
            {
                db.WorkDays.Add(w);
                SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CreateSchedule(Schedule s)
        {
            db.Schedules.Add(s);
            return SaveChanges();
        }

        /// update methods

        public bool UpdateEmployee(Person p)
        {
            db.People.AddOrUpdate(p);
            try
            {
               
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public bool UpdateWorkDay(WorkDay w)
        {
            try
            {
                db.WorkDays.AddOrUpdate(w);
                SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public bool UpdateDetail(Detail d)
        {
            try
            {
                db.Details.AddOrUpdate(d);
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }
        public bool UpdatePersontask(Task t)
        {
            t.ProjectId = 3;
            db.Tasks.AddOrUpdate(t);
            return SaveChanges();
        }

        public bool UpdateProjectTask(Task1 t)
        {
            db.Task1.AddOrUpdate(t);
            return SaveChanges();
        }

        public bool UpdateAcceptedProject(AcceptedProject ap)
        {
            db.AcceptedProjects.AddOrUpdate(ap);
            return SaveChanges();
        }

        /// other methods
        public bool RemoveDay(Schedule s)
        {
            var temp = db.Schedules.First(e => e.Id == s.Id);
            db.Schedules.Remove(temp);
            return SaveChanges();
        }
        public bool RemoveTask(Task t)
        {
            try
            {
                db.Tasks.Remove(db.Tasks.First(e=> e.Id == t.Id));
                return SaveChanges();
            }
            catch(Exception ex)
            {
                return false;
            }
           
        }
        public bool RemovePerson(Person p)
        {
            try
            {
                db.People.Remove(p);
                return SaveChanges();
                
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        public bool RemoveManagement(Person p)
        {
            try
            {
                foreach (var item in p.Managers)
                {
                    db.Managers.Remove(item);
                }
                foreach (var item in p.Managers1)
                {
                    db.Managers.Remove(item);
                }
                return SaveChanges();
            }
            catch (Exception ex)
            {

                return false;
            }
            
        }
        public bool RemoveDetail(Detail d)
        {
            db.Details.Remove(d);
            return SaveChanges();
        }

        public bool RemoveWorkDay(WorkDay w)
        {
            db.WorkDays.Remove(w);
            return SaveChanges();
        }
        public bool SaveChanges()
        {
           
            try
            {
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                
                return false;
            }
        }
    }
}
