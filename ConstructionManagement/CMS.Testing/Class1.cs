﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cms.DomainLogic;
using Xunit;
using Xunit.Abstractions;
namespace CMS.Testing
{
    public class Class1
    {
        DomainLogic x = new DomainLogic();
        private readonly ITestOutputHelper Output;

        public Class1(ITestOutputHelper output)
        {
            Output = output;
        }

        [Fact]
        public void GetEmployees()
        {
         
            Assert.True(x.GetEmployees().Any());
        }

        [Fact]
        public void GetPersonTasks()
        {
            Assert.True(x.GetPersonTasks().Any());
        }
        [Fact]
        public void Clock()
        {
            var z = new TimeClock();
            var temp = x.GetEmployees().First(e => e.ID == 6);
           Assert.True(z.Clock(new PersonDAO() { ID = 6 }) != null);;
        }
    }
}
