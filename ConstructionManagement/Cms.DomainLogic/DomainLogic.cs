﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using CMS.DataAccess;

namespace Cms.DomainLogic
{
    public class DomainLogic
    {
        EFAccess db = new EFAccess();
        public List<PersonDAO> GetEmployees()
        {
            var temp = new List<PersonDAO>();
            foreach (var item in db.GetEmployees())
            {
                temp.Add(item.FromDataAccess());
            }
            return temp;
        }

        public List<PersonTaskDao> GetPersonTasks()
        {
            var temp = new List<PersonTaskDao>();
            foreach (var item in db.GetEmployeeTasks())
            {
                temp.Add(item.FromDataAccess());
            }
            return temp;
        }

        public List<StatusDao> GetStatuses()
        {
            var temp = new List<StatusDao>();
            foreach (var item in db.GetStatuses())
            {
                temp.Add(item.FromDataAccess());
            }
            return temp;
        }

        public List<ProjectTaskDao> GetProjectTasks()
        {
            var temp = new List<ProjectTaskDao>();
            foreach (var item in db.GetProjectTasks())
            {
                temp.Add(item.FromDataAccess());
            }
            return temp;
        }

        public List<WorkDayDao> GetWorkDays()
        {
            var temp =  new List<WorkDayDao>();
            foreach (var item in db.GetWorkDays())
            {
                temp.Add(item.FromDataAccess());
            }
            return temp;
        }

        public List<DetailsDao> GetDetails()
        {
            var temp = new List<DetailsDao>();
            foreach (var item in db.GetEmployeeDetails())
            {
                temp.Add(item.FromDataAccess());
            }
            return temp;
        }

        public List<ScheduleDAO> GetSchedule(int id)
        {
            return db.GetSchedules(id).FromDataAccess();
        }

        public List<RoleDao> GetRoles()
        {
            return db.GetRoles().FromDataAccess();
        }

        public List<SiteDao> GetSites()
        {
            return db.GetProjectSites().FromDataAccess();
        }

        public bool AddEmployee(PersonDAO p, DetailsDao d)
        {
          
           return  db.CreateEmployee(p.ToDataAccess(),d.ToDataAccess());
        }

        public bool AddPersonTask(PersonTaskDao pt)
        {
            return db.CreateTask(pt.ToDataAccess());
        }

        public bool AddSchedule(ScheduleDAO s)
        {
            if (db.GetSchedules(s.EmployeeID).Any(e => e.DateWorking == s.DateWorking))
            {
                return false;
            }
            return db.CreateSchedule(s.ToDataAccess());
        }

        public bool ChangeRole(PersonDAO p,int roleId)
        {
            var temp = db.GetEmployees().First(e => e.ID == p.ID);
            temp.RoleID = roleId;
            return db.UpdateEmployee(temp);

        }

        public bool UpdatePersonTask(PersonTaskDao pt , int statusID)
        {
            pt.StatusID = statusID;
            return db.UpdatePersontask(pt.ToDataAccess());
        }

        public bool RemoveDay(Schedule s)
        {
            return db.RemoveDay(s);
        }

        public bool RemoveTask(PersonTaskDao pt)
        {
            return db.RemoveTask(pt.ToDataAccess());
        }

        public bool RemovePerson(PersonDAO p)
        {
            return db.RemovePerson(p.ToDataAccess());
        }

        public bool Terminate(int id)
        {
            var tasks = db.GetEmployeeTasks().Where(e => e.EmployeeId == id);
            var schedule = db.GetSchedules(id);
            var person = db.GetEmployees().First(e => e.ID == id);
            var details = db.GetEmployeeDetails().First(e => e.ID == person.DetailsID);
            var workdays = db.GetWorkDays().Where(e => e.EmployeeId == id);
            
            foreach (var item in tasks)
            {
                db.RemoveTask(item);
            }
            foreach (var item in schedule)
            {
                db.RemoveDay(item);
            }
            foreach (var item in workdays)
            {
                db.RemoveWorkDay(item);
            }
            db.RemoveManagement(person);
            db.RemovePerson(person);


            db.RemoveDetail(details);
           
            return true;

        }

     
    }
}
