﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CMS.DataAccess;

namespace Cms.DomainLogic
{
    public static class Mapper
    {
        /// <summary>
        /// From Data Access
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static PersonDAO FromDataAccess(this Person p)
        {
            bool clockedOut;
            try
            {
              clockedOut =
                    (bool)new EFAccess().GetWorkDays().Last(e => e.DateModified.Value.Date == DateTime.Today).Clockedout;
            }
            catch
            {
                clockedOut = true;
            }
            return new PersonDAO()
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                ID = p.ID,
                ClockedOut = clockedOut,
                RoleId = p.RoleID,
                DetailID = p.DetailsID
            };
        }

        public static PersonTaskDao FromDataAccess(this CMS.DataAccess.Task t)
        {
            return new PersonTaskDao()
            {
                EmployeeId = t.EmployeeId,
                HoursBudgeted = t.HoursBudgeted,
                HoursWorked = t.HoursWorked,
                Id = t.Id,
                Name = t.Name,
                StatusID = t.StatusID,
                ProjectId = t.ProjectId,
                DateModified = t.DateModified,

            };
        }

        public static ProjectTaskDao FromDataAccess(this Task1 t)
        {
            return new ProjectTaskDao()
            {
                Description = t.Description,
                Name = t.Name,
                HoursBudgeted = t.HoursBudgeted,
                HoursWorked = t.HoursWorked,
                Id = t.Id,
                ProjectID = t.Id,
                StatusID = t.StatusID,
                DateModified = t.DateModified,
            };
        }

        public static StatusDao FromDataAccess(this Status s)
        {
            return new StatusDao()
            {
                Id = s.Id,
                Name = s.Name
            };
        }

        public static WorkDayDao FromDataAccess(this WorkDay w)
        {
            return new WorkDayDao()
            {
                Clockedout = w.Clockedout,
                EmployeeId = w.EmployeeId,
                Id = w.Id,
                TimeIn = w.TimeIn,
                TimeOut = w.TimeOut,
                DateModified = w.DateModified
            };
        }

        public static DetailsDao FromDataAccess(this Detail d)
        {
            return new DetailsDao()
            {
                ID = d.ID,
                SiteID = d.SiteID,
                DateModified = d.DateModified,
                DateHired = d.DateHired,
                password = d.password,
                phone = d.phone,
                username = d.username,
                SSN = d.SSN,
                
            };
        }

        public static ScheduleDAO FromDataAccess(this Schedule s)
        {
            return new ScheduleDAO()
            {
                DateWorking = s.DateWorking,
                EmployeeID = s.EmployeeID,
                Id = s.Id,

            };
        }

        public static SiteDao FromDataAccess(this Site s)
        {
            return new SiteDao()
            {
                Id = s.Id,
                Name = s.Name
            };
        }

        public static RoleDao FromDataAccess(this Role r)
        {
            return new RoleDao()
            {
                ID = r.ID,
                Name = r.Name
            };
        }

        public static List<ScheduleDAO> FromDataAccess(this List<Schedule> s)
        {
            var temp = new List<ScheduleDAO>();
            foreach (var item in s)
            {
                temp.Add(item.FromDataAccess());
            }
            return temp;
        }

        public static List<RoleDao> FromDataAccess(this List<Role> r)
        {
            var temp = new List<RoleDao>();
            foreach (var item in r)
            {
                temp.Add(item.FromDataAccess());

            }
            return temp;
        }

        public static List<SiteDao> FromDataAccess(this List<Site> r)
        {
            var temp = new List<SiteDao>();
            foreach (var item in r)
            {
                temp.Add(item.FromDataAccess());
            }
            return temp;
        }
        ///
        /// To Data Access
        ///

        public static Person ToDataAccess(this PersonDAO p)
        {
            var clockedOut = new EFAccess().GetWorkDays().Last(e => e.DateModified.Value.Date == DateTime.Today).Clockedout;
            return new Person()
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                ID = p.ID,
                DateModified = DateTime.Now,
                Active = true,
                RoleID = p.RoleId
                

            };
        }

        public static CMS.DataAccess.Task ToDataAccess(this PersonTaskDao t)
        {
            return new CMS.DataAccess.Task()
            {
                EmployeeId = t.EmployeeId,
                HoursBudgeted = t.HoursBudgeted,
                HoursWorked = t.HoursWorked,
                Id = t.Id,
                Name = t.Name,
                StatusID = t.StatusID,
                DateModified = DateTime.Now,
                Active = true,
                ProjectId = t.ProjectId

            };
        }

        public static ProjectTaskDao ToDataAccess(this Task1 t)
        {
            return new ProjectTaskDao()
            {
                Description = t.Description,
                Name = t.Name,
                HoursBudgeted = t.HoursBudgeted,
                HoursWorked = t.HoursWorked,
                Id = t.Id,
                ProjectID = t.Id,
                StatusID = t.StatusID,
                DateModified = DateTime.Now,
                Active = true
            };
        }

        public static Status ToDataAccess(this StatusDao s)
        {
            return new Status()
            {
                Id = s.Id,
                Name = s.Name,
                DateModified = DateTime.Now,
                Active = true
            };
        }

        public static WorkDay ToDataAccess(this WorkDayDao w)
        {
            return new WorkDay()
            {
                Clockedout = w.Clockedout,
                EmployeeId = w.EmployeeId,
                Id = w.Id,
                TimeIn = w.TimeIn,
                TimeOut = w.TimeOut,
                DateModified = DateTime.Now,
                Active = true
            };
        }

        public static Detail ToDataAccess(this DetailsDao d)
        {
            return new Detail()
            {
                ID = d.ID,
                SiteID = d.SiteID,
                DateModified = DateTime.Now,
                DateHired = d.DateHired,
                password = d.password,
                phone = d.phone,
                username = d.username,
                SSN = d.SSN,
                Active = true
            };
        }
        public static Schedule ToDataAccess(this ScheduleDAO s)
        {
            return new Schedule()
            {
                DateWorking = s.DateWorking,
                EmployeeID = s.EmployeeID,
                Id = s.Id,
                DateModified = DateTime.Now,
                Active = true

            };
        }
        public static Role ToDataAccess (this RoleDao r)
        {
            return new Role()
            {
                ID = r.ID,
                Name = r.Name,
                Active = true,
                DateModified = DateTime.Now
            };
        }
    }
}
