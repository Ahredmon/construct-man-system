﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cms.DomainLogic
{
   public class PersonDAO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ID { get; set; }
        public bool ClockedOut { get; set; }
        public int RoleId { get; set; }
        public int DetailID { get; set; }
    }
}
