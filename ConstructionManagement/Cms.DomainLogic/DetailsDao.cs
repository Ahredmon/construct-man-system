﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cms.DomainLogic
{
    public class DetailsDao
    {
        public int ID { get; set; }
        public int SiteID { get; set; }
        public string SSN { get; set; }
        public string phone { get; set; }
        public string username { get; set; }
        public System.DateTime DateHired { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public Nullable<bool> Active { get; set; }

    }
}
