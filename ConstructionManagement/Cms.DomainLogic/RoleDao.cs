﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cms.DomainLogic
{
    public class RoleDao
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
