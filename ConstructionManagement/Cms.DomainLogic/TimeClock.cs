﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.DataAccess;

namespace Cms.DomainLogic
{
    public class TimeClock
    {
        EFAccess db = new EFAccess();
        DomainLogic dl = new DomainLogic();

        public PersonDAO Clock(PersonDAO p)
        {

            var x = new WorkDay()
            {
                Active = true,
                EmployeeId = p.ID,
                Clockedout = false,
                DateModified = DateTime.Now,
                TimeIn = DateTime.Now.TimeOfDay
            };

            try
            {
                var temp =
                    db.GetWorkDays().Last(e => e.DateModified.Value.Date == DateTime.Today && e.EmployeeId == p.ID);
                {
                    if (temp.TimeOut == null && temp.Clockedout == false)
                    {
                        
                        foreach (
                            var task in new EFAccess().GetEmployeeTasks().Where(e => e.EmployeeId == p.ID))
                        {
                            CalculateInterTotals(task.FromDataAccess());
                        }
                        {
}
                            temp.TimeOut = DateTime.Now.TimeOfDay;
                        temp.Clockedout = true;
                        db.UpdateWorkDay(temp);
                        p.ClockedOut = true;
                    }
                    else
                    {
                        db.CreateWorkday(x);
                        p.ClockedOut = false;
                    }
                }

            }
            catch (Exception ex)
            {
                db.CreateWorkday(x);
                p.ClockedOut = false;
            }
            return p;
        }



        public void CalculateTotals(PersonTaskDao x)
        {
           
            
                if (x.StatusID == 6)
                {
                    var temp = x.DateModified.Value.TimeOfDay;
                    var now = DateTime.Now.TimeOfDay;
                    var difference = now.Subtract(temp);
                    x.HoursWorked += difference.Duration().TotalHours;
                    var pt = db.GetProjectTasks().First(m => m.Id == x.ProjectId);
                    pt.HoursWorked += difference.Duration().TotalHours;
                    var project = db.GetProjects().First(m => m.Id == pt.ProjectID);
                    project.HoursWorked += difference.Duration().TotalHours;

                    x.StatusID = 8;
                    db.UpdatePersontask(x.ToDataAccess());
                    db.UpdateAcceptedProject(project);
                    db.UpdateProjectTask(pt);

                }
            }
        internal void CalculateInterTotals(PersonTaskDao x)
        {
            var Workday = dl.GetWorkDays().Last(e => e.EmployeeId == x.EmployeeId);

            if (x.StatusID == 6)
            {
                var temp = Workday.DateModified.Value.TimeOfDay;
                var now = DateTime.Now.TimeOfDay;
                var difference = now.Subtract(temp);
                x.HoursWorked += difference.Duration().TotalHours;
                var pt = db.GetProjectTasks().First(m => m.Id == x.ProjectId);
                pt.HoursWorked += difference.Duration().TotalHours;
                var project = db.GetProjects().First(m => m.Id == pt.ProjectID);
                project.HoursWorked += difference.Duration().TotalHours;

                x.StatusID = 8;
                db.UpdatePersontask(x.ToDataAccess());
                db.UpdateAcceptedProject(project);
                db.UpdateProjectTask(pt);

            }
        }
    }

    }



