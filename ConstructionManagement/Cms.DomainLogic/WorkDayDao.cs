﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cms.DomainLogic
{
    public class WorkDayDao
    {
        public int Id { get; set; }
        public System.TimeSpan TimeIn { get; set; }
        public Nullable<System.TimeSpan> TimeOut { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public Nullable<bool> Active { get; set; }
        public int EmployeeId { get; set; }
        public Nullable<bool> Clockedout { get; set; }
    }
}
