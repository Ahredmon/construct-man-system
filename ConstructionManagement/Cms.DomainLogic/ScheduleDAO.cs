﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cms.DomainLogic
{
    public class ScheduleDAO
    {
        public int Id { get; set; }
        public int EmployeeID { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public Nullable<bool> Active { get; set; }
        public System.DateTime DateWorking { get; set; }
    }
}
