﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Cms.DomainLogic;

namespace CMS.MVC.Helpers
{
    public static class DropDown
    {
        public static List<SelectListItem> Statuses()
        {
            var list= new List<SelectListItem>();
            foreach (var item in new DomainLogic().GetStatuses())
            {
                list.Add(new SelectListItem() {Text = item.Name, Value = item.Id.ToString()});
            }
            return list;
        }

        public static List<SelectListItem> ProjectTasks()
        {
            var list = new List<SelectListItem>();
            foreach (var item in new DomainLogic().GetProjectTasks())
            {
                list.Add(new SelectListItem() {Text = item.Name,Value = item.Id.ToString()});
            }
            return list;
        }

        public static List<SelectListItem> Roles()
        {
            var list = new List<SelectListItem>();
            foreach (var item in new DomainLogic().GetRoles())
            {
                list.Add(new SelectListItem()
                {
                    Text = item.Name,
                    Value = item.ID.ToString()
                });
            }
            return list;
        }

        public static List<SelectListItem> Sites()
        {
            var list = new List<SelectListItem>();
            foreach (var item in new DomainLogic().GetSites())
            {
                list.Add(new SelectListItem()
                {
                    Text = item.Name,
                    Value = item.Id.ToString()

                });

            }
            return list;
        }
    }
}