﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using Cms.DomainLogic;
using CMS.MVC.Helpers;
using CMS.MVC.Models;
using Newtonsoft.Json;

namespace CMS.MVC.Controllers
{
    public class userController : Controller
    {
        DomainLogic dl = new DomainLogic();
        // GET: user
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ActivateTask(int Id, int employeeId)
        {
            dl.UpdatePersonTask(dl.GetPersonTasks().First(e=>e.Id == Id), 6);
            return RedirectToAction("ListTasks",new {id = employeeId});
        }
        public ActionResult CompleteTask(int Id, int employeeId)
        {
          
           var x =  new TimeClock();
            x.CalculateTotals(dl.GetPersonTasks().First(e=>e.Id == Id));
            dl.UpdatePersonTask(dl.GetPersonTasks().First(e => e.Id == Id), 7);
            return RedirectToAction("ListTasks", new { id = employeeId });
        }

        public ActionResult ListTasks(int id)
        {
            ViewBag.Statuses = dl.GetStatuses();
            ViewBag.Employee = dl.GetEmployees().First(e => e.ID == id);
            var temp = dl.GetPersonTasks().ToViewModel().Where(p => p.EmployeeId == id);
            if (temp.Any())
                return View(temp);
            else
            {
                TempData["TaskEmpty"] = true;
            return RedirectToAction("Clock", new {id = id});
             }
    }

        public ActionResult EnterUsername()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EnterUsername(DetailMV d)
        {
            try
            {
               var details = dl.GetDetails().First(m => m.username.ToLower() == d.username.ToLower());
                var person = dl.GetEmployees().First(e => e.DetailID == details.ID);
                return RedirectToAction("ListTasks",new {id=person.ID});
            }
            catch (Exception)
            {
                ModelState.AddModelError("","User cannot be found");
                return View();
            }
        }

        public ActionResult Clock(int id)
        {
            var tc = new TimeClock();
            tc.Clock(dl.GetEmployees().First(e => e.ID == id));
            var HasTask = dl.GetPersonTasks().Any(e => e.EmployeeId == id);
            if (dl.GetPersonTasks().Any(e => e.EmployeeId == id))
                return RedirectToAction("ListTasks", new {id = id});
            else
            {
                if (!dl.GetPersonTasks().Any(e => e.EmployeeId == id))
                {
                    var temp = dl.GetEmployees().First(e => e.ID == id).ClockedOut;
                    if (!temp)
                    {
                        TempData["Message"] = "You have been clocked in! But you don't have any assigned tasks! Contact your manager.";
                    }
                    else
                    {
                        TempData["Message"] = "You have been clocked out!";
                    }

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("ListTasks", new { id = id });

        }
        public ActionResult JsonString()
        {
            var mytemp = dl.GetWorkDays().GroupBy(x => new {x.DateModified.Value.Date, x.EmployeeId}).Distinct();
            var temp = dl.GetWorkDays().GroupBy(e => e.DateModified.Value.Date).Distinct();
            
            var dateList = new List<DateCount>();

            foreach (var item in mytemp)
            {
                dateList.Add(new DateCount()
                {
                    Count = item.Count(),
                    Date = item.First().DateModified.Value.Date.ToString("d")

                });
            }
            var dateList2 = new List<DateCount>();
            foreach (var item in dateList.GroupBy(e => e.Date))
            {
                dateList2.Add(new DateCount()
                {
                    Date = item.First().Date,
                    Count = item.Count()
                });;
            }

            var Json = JsonConvert.SerializeObject(dateList2.Skip(dateList2.Count - 7));
            return Content(Json, "application/json");
        }
    }
}