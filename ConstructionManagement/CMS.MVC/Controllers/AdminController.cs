﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cms.DomainLogic;
using CMS.MVC;
using CMS.MVC.Helpers;
using CMS.MVC.Models;

namespace CMS.MVC.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        DomainLogic dl = new DomainLogic();  
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListEmployees()
        {
            return View(dl.GetEmployees().ToViewModel());

        }



        public ActionResult AddEmployee()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddEmployee(PersonDetailMV p)
        {
            if (ModelState.IsValid)
            {
                var employee = p.EmployeeMv;
                var details = p.DetailMV;
                try
                {
                   
                    dl.AddEmployee(employee.ToDomain(), details.ToDomain());
                    return RedirectToAction("ListEmployees");
                }
                catch (Exception)
                {
                    TempData["Message"] = "One or more validation errors occured, please try again";
                    ModelState.AddModelError("","A user already exists with this username!");
                    return RedirectToAction("AddEmployeeStyled");
                }
            }
            return RedirectToAction("AddEmployeeStyled");


        }
        public ActionResult AddEmployeeStyled()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddEmployeeStyled(PersonDetailMV p)
        {
            if (ModelState.IsValid)
            {
                var employee = p.EmployeeMv;
                var details = p.DetailMV;
                try
                {

                    dl.AddEmployee(employee.ToDomain(), details.ToDomain());
                    
                    return RedirectToAction("ListEmployees");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "A user already exists with this username!");
                    return View();
                }
            }
            return View();


        }

        public ActionResult ListSchedule(int id)
        {
            
            var temp = dl.GetSchedule(id).ToViewModel();
            ViewBag.EmployeeId = id;
            return View(temp);
        }

        public ActionResult AddSchedule(int id)
        {
            ViewBag.Employee = dl.GetEmployees().First(e => e.ID == id);
            return View();
        }
        [HttpPost]
        public ActionResult AddSchedule(ScheduleMV s)
        {
           s.EmployeeID =(int) TempData["EmployeeId"];
            if (!dl.AddSchedule(s.ToDomain()))
            {
                TempData["Error"] = "An employee cannot be schedule twice in the same day";
            }

            return RedirectToAction("ListSchedule", new { id = s.EmployeeID });
        }

        public ActionResult DeleteDay(int id, int sid)
        {
            var temp = dl.GetSchedule(id).First(e => e.Id == sid);

            if (dl.RemoveDay(temp.ToDataAccess()))
            {
                TempData["Error"] = "Day sucessfully removed";
            }
            else { }
            return RedirectToAction("ListSchedule",new {id = id});
        }

        public ActionResult AddTask(int id)
        {

            ViewBag.Employee = dl.GetEmployees().First(e => e.ID == id);
            return View();
        }
        [HttpPost]
        public ActionResult AddTask(PersonTaskMV p)
        {
            p.EmployeeId = (int)TempData["EmployeeId"];
            dl.AddPersonTask(p.ToDomain());
            return RedirectToAction("ListPersonTasks", new {id = p.EmployeeId});
        }
        
        public ActionResult ListPersonTasks(int id)
        {
            var temp = dl.GetPersonTasks().ToViewModel();
            ViewBag.EmployeeId = id;
            return View(temp.Where(e=>e.EmployeeId == id));

        }

        public ActionResult DeleteTask(int id)
        {
            var temp = dl.GetPersonTasks().First(e => e.Id == id);
            dl.RemoveTask(temp);
            return RedirectToAction("ListPersonTasks", new {id = temp.EmployeeId});
        }

        public ActionResult TerminateEmployee(int id)
        {
            dl.Terminate(id);
        
            return RedirectToAction("ListEmployees");
        }

        
    }
}