﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CMS.MVC.Models
{
    public class PersonTaskMV
    {
        public int Id { get; set; }
        [Required]
        public int ProjectId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        public int HoursBudgeted { get; set; }
        [Required]
        public double HoursWorked { get; set; }
     
        [Required]
        public int StatusID { get; set; }
        [Required]
        public string Name { get; set; }
    }
}