﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.MVC.Models
{
    public class PersonDetailMV
    {
        public EmployeeMV EmployeeMv { get; set; }
        public DetailMV DetailMV { get; set; }
    }
}