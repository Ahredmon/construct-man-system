﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CMS.MVC.Models
{
    public class LoginVM
    {
        [Required]

        public string username { get; set; }
        [DataType(DataType.Password)]
        [Required]
        public string password { get; set; }
    }
}