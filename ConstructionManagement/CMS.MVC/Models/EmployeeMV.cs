﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CMS.MVC.Models
{
    public class EmployeeMV
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public int ID { get; set; }
        [Required]
        public int RoleId { get; set; }
        [Required]
        public bool ClockedOut { get; set; }
    }
}