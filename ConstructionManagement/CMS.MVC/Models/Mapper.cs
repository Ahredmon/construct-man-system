﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Cms.DomainLogic;
using CMS.MVC.Models;


namespace CMS.MVC
{
    public static class Mapper
    {
        /// <summary>
        /// From Data Access
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        /// d
        public static PersonDAO ToDomain(this EmployeeMV p)
        {
         
            return new PersonDAO()
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                ID = p.ID,
                RoleId = p.RoleId
               
            };
        }

        public static PersonTaskDao ToDomain(this PersonTaskMV t)
        {
            return new PersonTaskDao()
            {
                EmployeeId = t.EmployeeId,
                HoursBudgeted = t.HoursBudgeted,
                HoursWorked = t.HoursWorked,
                Id = t.Id,
                Name = t.Name,
                StatusID = t.StatusID,
                ProjectId = t.ProjectId,
            
            };
        }

        public static ProjectTaskDao ToDomain(this ProjectTaskMV t)
        {
            return new ProjectTaskDao()
            {
                Description = t.Description,
                Name = t.Name,
                HoursBudgeted = t.HoursBudgeted,
                HoursWorked = t.HoursWorked,
                Id = t.Id,
                ProjectID = t.Id,
                StatusID = t.StatusID,
                DateModified = t.DateModified,
            };
        }

        public static StatusDao ToDomain(this StatusMV s)
        {
            return new StatusDao()
            {
                Id = s.Id,
                Name = s.Name
            };
        }

        public static WorkDayDao ToDomain(this WorkDayMV w)
        {
            return new WorkDayDao()
            {
                Clockedout = w.Clockedout,
                EmployeeId = w.EmployeeId,
                Id = w.Id,
                TimeIn = w.TimeIn,
                TimeOut = w.TimeOut,
                DateModified = w.DateModified
            };
        }

        public static DetailsDao ToDomain(this DetailMV d)
        {
            return new DetailsDao()
            {
                ID = d.ID,
                SiteID = d.SiteID,
                DateModified = d.DateModified,
                DateHired = d.DateHired,
                password = d.password,
                phone = d.phone,
                username = d.username,
                SSN = d.SSN,
            };
        }
        public static ScheduleDAO ToDomain(this ScheduleMV s)
        {
            return new ScheduleDAO()
            {
                EmployeeID = s.EmployeeID,
                Active = s.Active,
                DateModified = s.DateModified,
                DateWorking = s.DateWorking,
                Id = s.Id,

            };
        }
        ///
        /// To Data Access
        ///

        public static EmployeeMV ToClient(this PersonDAO p)
        {
           
            return new EmployeeMV()
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                ID = p.ID,
               ClockedOut = p.ClockedOut,
               RoleId = p.RoleId
            };
        }

        public static PersonTaskMV ToClient(this PersonTaskDao t)
        {
            return new PersonTaskMV()
            {
                EmployeeId = t.EmployeeId,
                HoursBudgeted = t.HoursBudgeted,
                HoursWorked = t.HoursWorked,
                Id = t.Id,
                Name = t.Name,
                StatusID = t.StatusID,
                ProjectId = t.ProjectId
            };
        }

        public static ProjectTaskDao ToClient(this ProjectTaskMV t)
        {
            return new ProjectTaskDao()
            {
                Description = t.Description,
                Name = t.Name,
                HoursBudgeted = t.HoursBudgeted,
                HoursWorked = t.HoursWorked,
                Id = t.Id,
                ProjectID = t.Id,
                StatusID = t.StatusID
            };
        }

        public static StatusMV ToClient(this StatusDao s)
        {
            return new StatusMV()
            {
                Id = s.Id,
                Name = s.Name
            };
        }

        public static WorkDayMV ToClient(this WorkDayDao w)
        {
            return new WorkDayMV()
            {
                Clockedout = w.Clockedout,
                EmployeeId = w.EmployeeId,
                Id = w.Id,
                TimeIn = w.TimeIn,
                TimeOut = w.TimeOut,
                DateModified = w.DateModified
            };
        }

        public static DetailMV ToClient(this DetailsDao d)
        {
            return new DetailMV()
            {
                DateModified = d.DateModified,
                Active = d.Active.Value,
                DateHired = d.DateHired,
                ID = d.ID,
                password = d.password,
                phone = d.phone,
                SiteID = d.SiteID,
                SSN = d.SSN,
                username = d.username
            };
        }

        public static ScheduleMV ToClient(this ScheduleDAO s)
        {
            return new ScheduleMV()
            {
                EmployeeID = s.EmployeeID,
                Active = s.Active,
                DateModified = s.DateModified,
                DateWorking = s.DateWorking,
                Id = s.Id,

            };
        }
    }
}
