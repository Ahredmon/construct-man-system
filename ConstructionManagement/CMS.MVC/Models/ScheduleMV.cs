﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CMS.MVC.Models
{
    public class ScheduleMV
    {
        public int Id { get; set; }
        [Required]
        public int EmployeeID { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public Nullable<bool> Active { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public System.DateTime DateWorking { get; set; }
    }
}