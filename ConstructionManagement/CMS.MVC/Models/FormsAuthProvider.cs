﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Cms.DomainLogic;

namespace CMS.MVC.Models
{
    public class FormsAuthProvider : IAuthProvider
    {
        DomainLogic dl = new DomainLogic();
        public int Authenticate(string username, string password)
        {
           

            try
            {
                var details = dl.GetDetails();
                var employees = dl.GetEmployees();
                var AuthenticatedDetails =
                    details.First(e => e.username.ToLower() == username.ToLower() && e.password == password);
                var AuthenticatedEmployee = employees.First(e => e.DetailID == AuthenticatedDetails.ID);

                var temprole = AuthenticatedEmployee.RoleId;
                if (temprole == 4)
                {

                    FormsAuthentication.SetAuthCookie(username, false);
                }
                return temprole;
            }
            catch (Exception)
            {

                return 0;
            }

        }
    }
}